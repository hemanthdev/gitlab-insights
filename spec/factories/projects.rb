FactoryBot.define do
  factory :project do
    group
    sequence(:path) { |n| "project-path-#{n}" }

    trait :dev do
      dev true
      override_path 'override-path'
    end
  end
end
