require 'rails_helper'

RSpec.describe SectionsController, type: :controller do
  let!(:group) { create(:group) }
  let!(:project) { create(:project, group: group) }

  describe "GET #show" do
    context 'access by path' do
      it "returns http success" do
        get :show, { group_id: group.path, id: project.path }
        expect(response).to have_http_status(:success)
      end

      context 'dev project' do
        before do
          project.update_attribute(:dev, true)
        end

        it 'returns 404' do
          expect do
            get :show, { group_id: group.path, id: project.path }
          end.to raise_error(ActionController::RoutingError)
        end
      end
    end
  end
end
