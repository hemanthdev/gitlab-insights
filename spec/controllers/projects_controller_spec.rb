require 'rails_helper'

RSpec.describe ProjectsController, type: :controller do
  let(:group) { create(:group) }
  let(:project) { create(:project, group: group) }

  describe "GET #show" do
    context 'access by path' do
      it "returns http success" do
        get :show, { group_id: group.path, id: project.path }
        expect(response).to have_http_status(:success)
      end

      context 'dev project' do
        before do
          project.update_attribute(:dev, true)
        end

        it 'returns 404' do
          expect do
            get :show, { group_id: group.path, id: project.path }
          end.to raise_error(ActionController::RoutingError)
        end
      end
    end
  end

  describe "GET chart views" do
    let(:chart_views) do
      [
        :bugs_by_stage,
        :bugs_by_severity,
        :bugs_by_priority,
        :feature_proposals_by_team,
        :issues_by_state,
        :regressions,
        :missed_deliverables,
        :bug_classification,
        :average_mrs_per_milestone
      ]
    end

    context 'access by path' do
      it "returns http success" do
        chart_views.each do |chart_view|
          get chart_view, { group_id: group.path, project_id: project.path }
          expect(response).to have_http_status(:success)
        end
      end

      context 'dev project' do
        before do
          project.update_attribute(:dev, true)
        end

        it 'returns 404' do
          chart_views.each do |chart_view|
            expect do
              get chart_view, { group_id: group.path, project_id: project.path }
            end.to raise_error(ActionController::RoutingError)
          end
        end
      end
    end
  end
end
