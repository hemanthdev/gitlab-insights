require 'rails_helper'
require 'support/period_finder_spec_helper'

RSpec.describe GitlabInsights::Finders::TimeToCloseIssuesFinder do
  start_point = Time.utc(2019, 6, 3)

  around do |example|
    Timecop.freeze(start_point) { example.run }
  end

  shared_examples_for "time to close issues per month finder" do
    let(:group) { create(:group) }
    let(:project) { create(:project, group: group) }
    let!(:issue1) { create(single_scope,
                      labels: ['bug','s1', 'customer'],
                      state: 'closed',
                      created_at: Date.new(2018,6,1),
                      closed_at: Date.new(2018,6,10),
                      project: project) }
    let!(:issue2) { create(single_scope,
                      labels: ['bug','s1', 'customer'],
                      state: 'closed',
                      created_at: Date.new(2018,6,1),
                      closed_at: Date.new(2018,6,20),
                      project: project) }
    let!(:issue3) { create(single_scope,
                      labels: ['bug','s1'],
                      state: 'closed',
                      created_at: Date.new(2018,6,1),
                      closed_at: Date.new(2018,6,30),
                      project: project) }
    let!(:issue4) { create(single_scope,
                      labels: ['bug','s2'],
                      state: 'closed',
                      created_at: Date.new(2018,6,1),
                      closed_at: Date.new(2018,6,10),
                      project: project) }

    subject { described_class.new(project, nil, association_scope) }

    context 'all bugs' do
      it 'has the correct count per month' do
        expected = [
          {
            label: 'June 2018',
            elements: {
              'bar': [
                { label: 'avg', count: 16.5 },
                { label: '85', count: 24.5 },
                { label: '95', count: 27.5 }
              ],
              'line': [
                { label: 'closed', count: 4}
              ]
            }
          }.with_indifferent_access,
        ]

        actual = subject.find({ filter_labels: ['bug']})

        expect(actual).to be_a(Array)

        assert_results_included(expected, actual, :label)
        assert_results_included(expected, actual, :elements)
      end
    end

    context 'all s1 bugs' do
      it 'has the correct count per month' do
        expected = [
          {
            label: 'June 2018',
            elements: {
              'bar': [
                { label: 'avg', count: 19.0 },
                { label: '85', count: 26.0 },
                { label: '95', count: 28.0 }
              ],
              'line': [
                { label: 'closed', count: 3}
              ]
            }
          }.with_indifferent_access,
        ]

        actual = subject.find({ filter_labels: ['bug', 's1']})

        expect(actual).to be_a(Array)

        assert_results_included(expected, actual, :label)
        assert_results_included(expected, actual, :elements)
      end
    end

    context 'all s1 customer bugs' do
      it 'has the correct count per month' do
        expected = [
          {
            label: 'June 2018',
            elements: {
              'bar': [
                { label: 'avg', count: 14.0 },
                { label: '85', count: 17.5 },
                { label: '95', count: 18.5 }
              ],
              'line': [
                { label: 'closed', count: 2}
              ]
            }
          }.with_indifferent_access,
        ]

        actual = subject.find({ filter_labels: ['bug', 's1', 'customer']})

        expect(actual).to be_a(Array)

        assert_results_included(expected, actual, :label)
        assert_results_included(expected, actual, :elements)
      end
    end
  end

  context 'issues' do
    let(:single_scope) { :issue }
    let(:association_scope) { :issues }

    include_examples "time to close issues per month finder"
  end
end
