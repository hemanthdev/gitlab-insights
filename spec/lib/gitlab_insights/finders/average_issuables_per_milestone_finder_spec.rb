require 'rails_helper'
require 'support/period_finder_spec_helper'

RSpec.describe GitlabInsights::Finders::AverageIssuablesPerMilestoneFinder do
  shared_examples_for "average issuables per milestone finder" do
    let(:group) { create(:group) }
    let(:project) { create(:project, group: group) }
    let(:milestone1) { { title: '10.1'} }
    let(:milestone2) { { title: '11.0'} }
    let(:milestone3) { { title: '11.1'} }
    let(:milestone4) { { title: '11.2'} }
    let(:milestone5) { { title: '11.3'} }
    let(:milestone6) { { title: '11.4'} }
    let(:milestone7) { { title: '11.5'} }
    let(:milestone8) { { title: '2017-08-04 invalid title' } }
    let(:milestone9) { { title: '11.10'} }
    let(:author1) { { username: 'author1' } }
    let(:author2) { { username: 'author2' } }
    let(:author3) { { username: 'author3' } }
    let!(:issuable1) { create(single_scope, :open, project: project, milestone: milestone1, author: author1) }
    let!(:issuable2) { create(single_scope, :closed, project: project, milestone: milestone1, author: author2) }
    let!(:issuable3) { create(single_scope, :open, project: project, milestone: milestone2, author: author1) }
    let!(:issuable4) { create(single_scope, :open, project: project, milestone: milestone2, author: author2, labels: ['Community contribution']) }
    let!(:issuable5) { create(single_scope, :closed, project: project, milestone: milestone1, author: author1, labels: ['Community contribution']) }
    let!(:issuable6) { create(single_scope, :closed, project: project, milestone: milestone1, author: author3) }

    let!(:issuable7) { create(single_scope, :closed, project: project, milestone: milestone3, author: author3) }
    let!(:issuable8) { create(single_scope, :closed, project: project, milestone: milestone4, author: author3) }
    let!(:issuable9) { create(single_scope, :closed, project: project, milestone: milestone5, author: author3) }
    let!(:issuable10) { create(single_scope, :closed, project: project, milestone: milestone6, author: author3) }
    let!(:issuable11) { create(single_scope, :closed, project: project, milestone: milestone7, author: author3) }

    let!(:issuable12) { create(single_scope, :closed, project: project, milestone: milestone8, author: author3) }

    let!(:issuable13) { create(single_scope, :closed, project: project, milestone: milestone9, author: author3) }

    let!(:no_milestone_issuable) { create(single_scope, :closed, project: project, author: author1) }

    subject { described_class.new(project, nil, association_scope) }

    context 'all mrs' do
      it 'has the correct count per milestone' do
        expected = [
          {
            label: '10.1',
            elements: {
              'bar': [{"label"=>"MRs Merged Per Milestone", "count"=>1.33}],
              'line': [{"label"=>"Rolling Average", "count"=>nil}]
            }
          }.with_indifferent_access,
          {
            label: '11.0',
            elements: {
              'bar': [{"label"=>"MRs Merged Per Milestone", "count"=>1.0}],
              'line': [{"label"=>"Rolling Average", "count"=>nil}]
            }
          }.with_indifferent_access,
          {
            label: '11.1',
            elements: {
              'bar': [{"label"=>"MRs Merged Per Milestone", "count"=>1.0}],
              'line': [{"label"=>"Rolling Average", "count"=>nil}]
            }
          }.with_indifferent_access,
          {
            label: '11.2',
            elements: {
              'bar': [{"label"=>"MRs Merged Per Milestone", "count"=>1.0}],
              'line': [{"label"=>"Rolling Average", "count"=>nil}]
            }
          }.with_indifferent_access,
          {
            label: '11.3',
            elements: {
              'bar': [{"label"=>"MRs Merged Per Milestone", "count"=>1.0}],
              'line': [{"label"=>"Rolling Average", "count"=>nil}]
            }
          }.with_indifferent_access,
          {
            label: '11.4',
            elements: {
              'bar': [{"label"=>"MRs Merged Per Milestone", "count"=>1.0}],
              'line': [{"label"=>"Rolling Average", "count"=>1.06}]
            }
          }.with_indifferent_access,
          {
            label: '11.5',
            elements: {
              'bar': [{"label"=>"MRs Merged Per Milestone", "count"=>1.0}],
              'line': [{"label"=>"Rolling Average", "count"=>1.0}]
            }
          }.with_indifferent_access,
          {
            label: '11.10',
            elements: {
              'bar': [{"label"=>"MRs Merged Per Milestone", "count"=>1.0}],
              'line': [{"label"=>"Rolling Average", "count"=>nil}]
            }
          }.with_indifferent_access
        ]

        actual = subject.find({})

        expect(actual).to be_a(Array)

        assert_results(expected, actual, :label)
        assert_results(expected, actual, :elements)
      end

      it 'does not include invalid milestone' do
        milestone_titles = subject.find({}).map do |result|
          result[:label]
        end

        expect(milestone_titles).not_to include(milestone8[:title])
      end
    end

    context 'without community contributions' do
      it 'has the correct count per milestone' do
        expected = [
          {
            label: '10.1',
            elements: {
              'bar': [{"label"=>"MRs Merged Per Milestone", "count"=>1.0}],
              'line': [{"label"=>"Rolling Average", "count"=>nil}]
            }
          }.with_indifferent_access,
          {
            label: '11.0',
            elements: {
              'bar': [{"label"=>"MRs Merged Per Milestone", "count"=>1.0}],
              'line': [{"label"=>"Rolling Average", "count"=>nil}]
            }
          }.with_indifferent_access,
          {
            label: '11.1',
            elements: {
              'bar': [{"label"=>"MRs Merged Per Milestone", "count"=>1.0}],
              'line': [{"label"=>"Rolling Average", "count"=>nil}]
            }
          }.with_indifferent_access,
          {
            label: '11.2',
            elements: {
              'bar': [{"label"=>"MRs Merged Per Milestone", "count"=>1.0}],
              'line': [{"label"=>"Rolling Average", "count"=>nil}]
            }
          }.with_indifferent_access,
          {
            label: '11.3',
            elements: {
              'bar': [{"label"=>"MRs Merged Per Milestone", "count"=>1.0}],
              'line': [{"label"=>"Rolling Average", "count"=>nil}]
            }
          }.with_indifferent_access,
          {
            label: '11.4',
            elements: {
              'bar': [{"label"=>"MRs Merged Per Milestone", "count"=>1.0}],
              'line': [{"label"=>"Rolling Average", "count"=>1.0}]
            }
          }.with_indifferent_access,
          {
            label: '11.5',
            elements: {
              'bar': [{"label"=>"MRs Merged Per Milestone", "count"=>1.0}],
              'line': [{"label"=>"Rolling Average", "count"=>1.0}]
            }
          }.with_indifferent_access,
          {
            label: '11.10',
            elements: {
              'bar': [{"label"=>"MRs Merged Per Milestone", "count"=>1.0}],
              'line': [{"label"=>"Rolling Average", "count"=>nil}]
            }
          }.with_indifferent_access
        ]

        actual = subject.find({ exclude_community_contributions: true })

        expect(actual).to be_a(Array)

        assert_results(expected, actual, :label)
        assert_results(expected, actual, :elements)
      end
    end
  end

  context 'issues' do
    let(:single_scope) { :issue }
    let(:association_scope) { :issues }

    include_examples "average issuables per milestone finder"
  end

  context 'merge requests' do
    let(:single_scope) { :merge_request }
    let(:association_scope) { :merge_requests }

    include_examples "average issuables per milestone finder"
  end
end
