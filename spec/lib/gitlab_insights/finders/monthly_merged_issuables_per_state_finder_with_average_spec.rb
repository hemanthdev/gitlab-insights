require 'rails_helper'
require 'support/period_finder_spec_helper'

RSpec.describe GitlabInsights::Finders::MonthlyMergedIssuablesPerStateFinderWithAverage do
  shared_examples_for "Monthly Merged MRs with Average" do
    let(:group) { create(:group) }
    let(:project) { create(:project, group: group) }
    let(:author1) { { username: 'author1' } }
    let(:author2) { { username: 'author2' } }
    let(:author3) { { username: 'author3' } }
    let!(:issuable1) { create(single_scope, state: 'merged', merged_at: Date.new(2018,6,10), author: author1, project: project) }
    let!(:issuable2) { create(single_scope, state: 'merged', merged_at: Date.new(2018,7,10), author: author2, project: project) }
    let!(:issuable3) { create(single_scope, state: 'merged', merged_at: Date.new(2018,8,10), author: author1, project: project) }
    let!(:issuable4) { create(single_scope, state: 'merged', merged_at: Date.new(2018,9,10), author: author1, project: project) }
    let!(:community_issuable1) { create(single_scope, state: 'merged', merged_at: Date.new(2018,9,10), author: author2, labels: ['Community contribution'], project: project) }
    let!(:issuable5) { create(single_scope, state: 'merged', merged_at: Date.new(2018,10,10), author: author1, project: project) }
    let!(:community_issuable2) { create(single_scope, state: 'merged', merged_at: Date.new(2018,10,10), author: author2, labels: ['Community contribution'], project: project) }
    let!(:issuable6) { create(single_scope, state: 'merged', merged_at: Date.new(2018,11,10), author: author3, project: project) }
    let!(:issuable7) { create(single_scope, state: 'merged', merged_at: Date.new(2018,12,10), author: author3, project: project) }

    subject { described_class.new(project, nil, association_scope) }

    context 'all mrs' do
      it 'has the correct count per month' do
        expected = [
          {
            label: 'June 2018',
            elements: {
              'bar': [{"label"=>"MRs Merged Per Month", "count"=>1.0}],
              'line': [{"label"=>"Rolling Average", "count"=>nil}]
            }
          }.with_indifferent_access,
          {
            label: 'July 2018',
            elements: {
              'bar': [{"label"=>"MRs Merged Per Month", "count"=>1.0}],
              'line': [{"label"=>"Rolling Average", "count"=>nil}]
            }
          }.with_indifferent_access,
          {
            label: 'August 2018',
            elements: {
              'bar': [{"label"=>"MRs Merged Per Month", "count"=>1.0}],
              'line': [{"label"=>"Rolling Average", "count"=>nil}]
            }
          }.with_indifferent_access,
          {
            label: 'September 2018',
            elements: {
              'bar': [{"label"=>"MRs Merged Per Month", "count"=>2.0}],
              'line': [{"label"=>"Rolling Average", "count"=>nil}]
            }
          }.with_indifferent_access,
          {
            label: 'October 2018',
            elements: {
              'bar': [{"label"=>"MRs Merged Per Month", "count"=>2.0}],
              'line': [{"label"=>"Rolling Average", "count"=>nil}]
            }
          }.with_indifferent_access,
          {
            label: 'November 2018',
            elements: {
              'bar': [{"label"=>"MRs Merged Per Month", "count"=>1.0}],
              'line': [{"label"=>"Rolling Average", "count"=>1.33}]
            }
          }.with_indifferent_access,
          {
            label: 'December 2018',
            elements: {
              'bar': [{"label"=>"MRs Merged Per Month", "count"=>1.0}],
              'line': [{"label"=>"Rolling Average", "count"=>nil}]
            }
          }.with_indifferent_access
        ]

        actual = subject.find({})

        expect(actual).to be_a(Array)

        assert_results(expected, actual, :label)
        assert_results(expected, actual, :elements)
      end
    end

    context 'without community contributions' do
      it 'has the correct count per milestone' do
                expected = [
          {
            label: 'June 2018',
            elements: {
              'bar': [{"label"=>"MRs Merged Per Month", "count"=>1.0}],
              'line': [{"label"=>"Rolling Average", "count"=>nil}]
            }
          }.with_indifferent_access,
          {
            label: 'July 2018',
            elements: {
              'bar': [{"label"=>"MRs Merged Per Month", "count"=>1.0}],
              'line': [{"label"=>"Rolling Average", "count"=>nil}]
            }
          }.with_indifferent_access,
          {
            label: 'August 2018',
            elements: {
              'bar': [{"label"=>"MRs Merged Per Month", "count"=>1.0}],
              'line': [{"label"=>"Rolling Average", "count"=>nil}]
            }
          }.with_indifferent_access,
          {
            label: 'September 2018',
            elements: {
              'bar': [{"label"=>"MRs Merged Per Month", "count"=>1.0}],
              'line': [{"label"=>"Rolling Average", "count"=>nil}]
            }
          }.with_indifferent_access,
          {
            label: 'October 2018',
            elements: {
              'bar': [{"label"=>"MRs Merged Per Month", "count"=>1.0}],
              'line': [{"label"=>"Rolling Average", "count"=>nil}]
            }
          }.with_indifferent_access,
          {
            label: 'November 2018',
            elements: {
              'bar': [{"label"=>"MRs Merged Per Month", "count"=>1.0}],
              'line': [{"label"=>"Rolling Average", "count"=>1.0}]
            }
          }.with_indifferent_access,
          {
            label: 'December 2018',
            elements: {
              'bar': [{"label"=>"MRs Merged Per Month", "count"=>1.0}],
              'line': [{"label"=>"Rolling Average", "count"=>nil}]
            }
          }.with_indifferent_access
        ]

        actual = subject.find({ exclude_community_contributions: true })

        expect(actual).to be_a(Array)

        assert_results(expected, actual, :label)
        assert_results(expected, actual, :elements)
      end
    end
  end

  context 'merge requests' do
    let(:single_scope) { :merge_request }
    let(:association_scope) { :merge_requests }

    include_examples "Monthly Merged MRs with Average"
  end
end
