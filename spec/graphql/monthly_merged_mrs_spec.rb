require 'rails_helper'

RSpec.describe GitlabInsightsDashboardSchema do
  context 'monthly_merged_mrs field' do
    let(:view_string) { 'monthly_merged_mrs' }
    let(:issuable_timestamp_field) { :merged_at }
    let(:issuable_status) { :merged }

    include_examples 'per month query'

    context 'mrs' do
      include_examples 'mr query' do
        include_examples 'monthly merged mrs tests'
      end
    end
  end
end
