require 'rails_helper'

RSpec.describe GitlabInsightsDashboardSchema do
  let(:view_string) { 'average_issuables_per_milestone' }
  let(:issuable_timestamp_field) { :created_at }
  let(:query_period) { :day }
  let(:query_period_format) { "%d %b %y" }

  context 'average_issuables_per_milestone field' do
    context 'MRs' do
      context 'merged' do
        let(:issuable_status) { :merged }

        include_examples 'mr query' do
          include_examples 'average issuables per milestone'
        end
      end

      context 'opened' do
        let(:issuable_status) { :open }

        include_examples 'mr query' do
          include_examples 'average issuables per milestone'
        end
      end

      context 'closed' do
        let(:issuable_status) { :closed }

        include_examples 'mr query' do
          include_examples 'average issuables per milestone'
        end
      end
    end

    context 'issues' do
      context 'opened' do
        let(:issuable_status) { :open }

        include_examples 'issue query' do
          include_examples 'average issuables per milestone'
        end
      end

      context 'closed' do
        let(:issuable_status) { :closed }

        include_examples 'issue query' do
          include_examples 'average issuables per milestone'
        end
      end
    end
  end
end
