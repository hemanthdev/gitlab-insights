require 'rails_helper'

RSpec.describe GitlabInsightsDashboardSchema do
  context 'issuables_per_state field' do
    context 'MRs merged' do
      let(:issuable_status) { :merged }
      let(:issuable_timestamp_field) { :merged_at }
      let(:issuable_status_enum_string) { issuable_status.to_s.titleize }

      context 'per month' do
        let(:view_string) { 'issuables_merged_per_month' }

        include_examples 'per month query'

        include_examples 'mr query' do
          include_examples 'issuables per status per period'
        end
      end

      context 'per week' do
        let(:view_string) { 'issuables_merged_per_week' }

        include_examples 'per week query'

        include_examples 'mr query' do
          include_examples 'issuables per status per period'
        end
      end
    end

    context 'issuables created' do
      let(:issuable_status) { :open }
      let(:issuable_timestamp_field) { :created_at }

      context 'per month' do
        let(:view_string) { 'issuables_created_per_month' }

        include_examples 'per month query'

        context 'mrs' do
          include_examples 'mr query' do
            include_examples 'issuables per status per period'
          end
        end

        context 'issues' do
          include_examples 'issue query' do
            include_examples 'issuables per status per period'
          end
        end
      end

      context 'per day' do
        let(:view_string) { 'issuables_created_per_day' }

        include_examples 'per day query'

        context 'mrs' do
          include_examples 'mr query' do
            include_examples 'issuables per status per period'
          end
        end

        context 'issues' do
          include_examples 'issue query' do
            include_examples 'issuables per status per period'
          end
        end
      end
    end

    context 'issuables closed' do
      let(:issuable_status) { :closed }
      let(:issuable_timestamp_field) { :closed_at }

      context 'per month' do
        let(:view_string) { 'issuables_closed_per_month' }

        include_examples 'per month query'

        context 'mrs' do
          include_examples 'mr query' do
            include_examples 'issuables per status per period'
          end
        end

        context 'issues' do
          include_examples 'issue query' do
            include_examples 'issuables per status per period'
          end
        end
      end

      context 'per day' do
        let(:view_string) { 'issuables_closed_per_day' }

        include_examples 'per day query'

        context 'mrs' do
          include_examples 'mr query' do
            include_examples 'issuables per status per period'
          end
        end

        context 'issues' do
          include_examples 'issue query' do
            include_examples 'issuables per status per period'
          end
        end
      end
    end
  end
end
