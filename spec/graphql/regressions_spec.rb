require 'rails_helper'

RSpec.describe GitlabInsightsDashboardSchema do
  context 'regressions field' do
    let(:view_string) { 'regressions' }

    it_behaves_like 'issuable_per_label view'
  end
end
