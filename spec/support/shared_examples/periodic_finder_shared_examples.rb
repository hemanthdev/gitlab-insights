shared_examples_for 'periodic finder' do |field|
  context 'period_limit parameter' do
    context 'default' do
      it 'returns all results' do
        expect(subject.length).to eq(expected.length)

        assert_results(expected, subject, field)
      end
    end

    context 'with value' do
      before do
        opts.merge!(period_limit: 1)
      end

      it 'reduces the results' do
        expect(subject.length).to eq(1)

        assert_results([expected.last], subject, field)
      end
    end
  end
end
