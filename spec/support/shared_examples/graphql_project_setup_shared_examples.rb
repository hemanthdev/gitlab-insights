shared_examples_for 'graphql project setup' do
  let(:timestamp_base) { Date.new(2018,1,1) }

  let(:issuable_scope) { association_scope.to_s.titleize.split.join }
  let(:issuable_status_enum_string) { issuable_status.to_s.titleize }
  let(:query_period_normalizer) { "beginning_of_#{query_period.to_s}".to_sym }

  let(:group_path) { 'group-path' }
  let(:project_path1) { 'project1' }
  let(:project_path2) { 'project2' }
  let(:filter_labels) { ['filter'] }
  let(:collection_label1) { 'collect1' }
  let(:collection_label2) { 'collect2' }
  let(:state_string) { "state:#{issuable_status_enum_string}" }

  let(:author1) { { username: 'author1' } }
  let(:author2) { { username: 'author2' } }
  let(:milestone1) { { title: '11.0' } }
  let(:milestone2) { { title: '12.0' } }

  let(:group) { create(:group, path: group_path) }
  let(:project1) { create(:project, group: group, path: project_path1) }
  let(:project2) { create(:project, group: group, path: project_path2) }
  let!(:issuable1) do
    create(single_scope, issuable_status,
      issuable_timestamp_field => increment_date,
      labels: Array([*filter_labels, collection_label1, 'Community contribution']),
      author: author1,
      milestone: milestone1,
      project: project1)
  end
  let!(:issuable2) do
    create(single_scope, issuable_status,
      issuable_timestamp_field => increment_date,
      labels: Array([*filter_labels, collection_label2, 'Community contribution']),
      author: author1,
      milestone: milestone2,
      project: project1)
  end
  let!(:issuable3) do
    create(single_scope, issuable_status,
      issuable_timestamp_field => increment_date,
      labels: Array(filter_labels),
      author: author2,
      milestone: milestone1,
      project: project1)
  end
  let!(:issuable4) do
    create(single_scope, issuable_status,
      issuable_timestamp_field => increment_date,
      labels: Array([*filter_labels, collection_label2]),
      author: author2,
      milestone: milestone2,
      project: project2)
  end

  def increment_date
    @latest_date ||= timestamp_base
    @latest_date = @latest_date + 1.send(query_period)
  end

  def period_title_string(issuable)
    issuable.send(issuable_timestamp_field).send(query_period_normalizer).strftime(query_period_format)
  end
end
