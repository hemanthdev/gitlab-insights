shared_examples_for 'monthly merged mrs tests' do
  include_examples 'graphql project setup'

  context 'authored by all' do
    let(:exclude_community_contributions) { 'false' }

    context 'project-level' do
      let(:query_string) do
        <<-QUERY
          {
            project(group_path: "#{group_path}", project_path: "#{project1.path}") {
              #{view_string}(#{state_string}, issuable_scope: #{issuable_scope}, exclude_community_contributions: #{exclude_community_contributions}) {
                labels
                line_dataset {
                  type
                  label
                  data
                }
                bar_dataset {
                  type
                  label
                  data
                }
              }
            }
          }
        QUERY
      end

      let(:expected) do
        {
          "data": {
            "project": {
              view_string => {
                "labels":[
                  issuable1.merged_at.strftime(query_period_format),
                  issuable2.merged_at.strftime(query_period_format),
                  issuable3.merged_at.strftime(query_period_format)
                ],
                "line_dataset": [{
                  type: 'line',
                  label: 'Rolling Average',
                  data: [nil,nil,nil]
                }],
                "bar_dataset": [{
                  type: 'bar',
                  label: 'MRs Merged Per Month',
                  data: [1.0,1.0,1.0]
                }]
              }
            }
          }
        }.to_json
      end

      it_behaves_like 'graphql query endpoint'
    end

    context 'group-level' do
      let(:query_string) do
        <<-QUERY
          {
            group(group_path: "#{group_path}") {
              #{view_string}(#{state_string}, issuable_scope: #{issuable_scope}, exclude_community_contributions: #{exclude_community_contributions}) {
                labels
                line_dataset {
                  type
                  label
                  data
                }
                bar_dataset {
                  type
                  label
                  data
                }
              }
            }
          }
        QUERY
      end
      let(:expected) do
        {
          "data": {
            "group": {
              view_string => {
                "labels":[
                  issuable1.merged_at.strftime(query_period_format),
                  issuable2.merged_at.strftime(query_period_format),
                  issuable3.merged_at.strftime(query_period_format),
                  issuable4.merged_at.strftime(query_period_format)
                ],
                "line_dataset": [{
                  type: 'line',
                  label: 'Rolling Average',
                  data: [nil,nil,nil,nil]
                }],
                "bar_dataset": [{
                  type: 'bar',
                  label: 'MRs Merged Per Month',
                  data: [1.0,1.0,1.0,1.0]
                }]
              }
            }
          }
        }.to_json
      end

      it_behaves_like 'graphql query endpoint'
    end
  end

  context 'exclude_community_contributions' do
    let(:exclude_community_contributions) { 'true' }

    context 'project-level' do
      let(:query_string) do
        <<-QUERY
          {
            project(group_path: "#{group_path}", project_path: "#{project1.path}") {
              #{view_string}(#{state_string}, issuable_scope: #{issuable_scope}, exclude_community_contributions: #{exclude_community_contributions}) {
                labels
                line_dataset {
                  type
                  label
                  data
                }
                bar_dataset {
                  type
                  label
                  data
                }
              }
            }
          }
        QUERY
      end

      let(:expected) do
        {
          "data": {
            "project": {
              view_string => {
                "labels":[
                  issuable3.merged_at.strftime(query_period_format)
                ],
                "line_dataset": [{
                  type: 'line',
                  label: 'Rolling Average',
                  data: [nil]
                }],
                "bar_dataset": [{
                  type: 'bar',
                  label: 'MRs Merged Per Month',
                  data: [1.0]
                }]
              }
            }
          }
        }.to_json
      end

      it_behaves_like 'graphql query endpoint'
    end

    context 'group-level' do
      let(:query_string) do
        <<-QUERY
          {
            group(group_path: "#{group_path}") {
              #{view_string}(#{state_string}, issuable_scope: #{issuable_scope}, exclude_community_contributions: #{exclude_community_contributions}) {
                labels
                line_dataset {
                  type
                  label
                  data
                }
                bar_dataset {
                  type
                  label
                  data
                }
              }
            }
          }
        QUERY
      end
      let(:expected) do
        {
          "data": {
            "group": {
              view_string => {
                "labels":[
                  issuable3.merged_at.strftime(query_period_format),
                  issuable4.merged_at.strftime(query_period_format)
                ],
                "line_dataset": [{
                  type: 'line',
                  label: 'Rolling Average',
                  data: [nil,nil]
                }],
                "bar_dataset": [{
                  type: 'bar',
                  label: 'MRs Merged Per Month',
                  data: [1.0,1.0]
                }]
              }
            }
          }
        }.to_json
      end

      it_behaves_like 'graphql query endpoint'
    end
  end
end
