require 'rails_helper'

RSpec.describe MergeRequest, type: :model do
  describe '#url' do
    let(:merge_request) { build(:merge_request) }

    it 'returns the URL to the merge request' do
      expect(merge_request.url)
        .to eq("https://gitlab.com/#{merge_request.project.full_path}/merge_requests/#{merge_request.iid}")
    end
  end

  describe '#throughput' do
    let(:merge_request) { build(:merge_request, labels: labels) }

    context 'when there is no throughput label' do
      let(:labels) { [] }

      it 'returns nil' do
        expect(merge_request.throughput).to be_nil
      end
    end

    context 'when there is a throughput label' do
      let(:labels) { %w[bug] }

      it 'returns the throughput label' do
        expect(merge_request.throughput).to eq('bug')
      end
    end

    context 'when there are multiple throughput labels' do
      let(:labels) { %w[backstage feature] }

      it 'picks feature over backstage' do
        expect(merge_request.throughput).to eq('feature')
      end
    end
  end
end
