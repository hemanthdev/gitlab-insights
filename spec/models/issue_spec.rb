require 'rails_helper'

RSpec.describe Issue, type: :model do
  describe '#url' do
    let(:issue) { build(:issue) }

    it 'returns the URL to the issue' do
      expect(issue.url)
        .to eq("https://gitlab.com/#{issue.project.full_path}/issues/#{issue.iid}")
    end
  end

  describe '#throughput' do
    let(:issue) { build(:issue, labels: labels) }

    context 'when there is no throughput label' do
      let(:labels) { [] }

      it 'returns nil' do
        expect(issue.throughput).to be_nil
      end
    end

    context 'when there is a throughput label' do
      let(:labels) { %w[feature] }

      it 'returns the throughput label' do
        expect(issue.throughput).to eq('feature')
      end
    end

    context 'when there are multiple throughput labels' do
      let(:labels) { %w[security bug] }

      it 'picks security over bug' do
        expect(issue.throughput).to eq('security')
      end
    end
  end
end
