require "rails_helper"

RSpec.describe "Routing", :type => :request do
  let(:group_path) { 'regular-group' }
  let(:project_path) { 'regular-project' }
  let!(:group) { create(:group, path: group_path) }
  let!(:project) { create(:project, path: project_path, group: group) }

  shared_examples_for 'successful routed request' do
    it 'responds success for group' do
      get "/groups/#{group.path}"
      expect(response).to be_success
    end

    it 'responds success for project' do
      get "/groups/#{group.path}/projects/#{project.path}"
      expect(response).to be_success
    end
  end

  it_behaves_like 'successful routed request'

  context 'path containing dots' do
    let(:group_path) { 'dot.group' }
    let(:project_path) { 'dot.project' }

    it_behaves_like 'successful routed request'
  end
end
