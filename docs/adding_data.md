### Data

The web app is publicly accessible and there is currently no admin interface to add resources. Therefore, new projects and data adjustments happen via the rails console for the environment

#### Accessing the Rails console

- SSH into the server: `ssh root@quality-dashboard.gitlap.com`
- List the containers available: `docker ps`
- Find the container running in the environment where you want to adjust data:
  - `insights-production` for production container
  - `insights-{feature-branch-ref}` for review apps containers
- Start a Bash session terminal in that container: `docker exec -it insights-production /bin/bash`
- Start the rails console: `bundle exec rails c`

#### Adding a new project for GitLab.com

Projects are nested inside groups

```
group = Group.find_by(path: 'gitlab-org')
project = group.projects.find_by(path: 'gitlab')

new_project = group.projects.create(path: 'new-path')
```

#### Adding a new project for the dev instance

For the dev instance an overridden path must be provided that mirrors the project's full path on dev:

```
group/sub-group/project
```

Projects are nested inside groups

```
group = Group.find_by(path: 'gitlab-org')

new_project = group.projects.create(path: 'gitlab-dev', dev: true, override_path: 'dev-group/dev-project')
```

These `dev` projects will not be listed or visible in the group's project. But their resources will still contribute to the group level charts!

#### Adding a new project for a subgroup

Subgroup project can be added, just make sure that the project's path includes all of the subgroups under the top-level group:

```
group/sub-group/project
```

Projects are nested inside groups

```
group = Group.find_by(path: 'gitlab-org')

new_project = group.projects.create(path: 'quality/triage-ops')
```

These projects will be accessible beneath the group's path. A subgroup will not be created. For this example, it can be found in the insights group `gitlab-org`. The projects resources will be included in that group's charts
