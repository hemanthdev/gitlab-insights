## Database

Database is Postgres

### Production environment

- name: `gitlab_insights_dashboard`

The production database is updated by a scheduled pipeline

### Review environment

- name: `gitlab_insights_dashboard_review`

The review database is not currently updated automatically and is initiated as a manual action for a feature branch pipeline
