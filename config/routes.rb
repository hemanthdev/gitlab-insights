Rails.application.routes.draw do
  resources :groups, only: [:index, :show], constraints: { id: /[^\/]+/ } do
    resources :projects, only: [:index, :show], constraints: { id: /\S+/ } do
      get :bugs_by_team, to: redirect { |path_params, req| "/groups/#{path_params[:group_id]}/projects/#{path_params[:project_id]}/bugs_by_stage" }
      get :bugs_by_stage
      get :bugs_by_severity
      get :bugs_by_priority
      get :bugs_past_slo
      get :feature_proposals_by_team
      get :issues_by_state
      get :mrs_by_state
      get :regressions
      get :completed_deliverables_per_milestone
      get :missed_deliverables
      get :bug_classification
      get :developer_productivity_metrics, to: redirect { |path_params, req| "/groups/#{path_params[:group_id]}/projects/#{path_params[:project_id]}/average_mrs_per_milestone" }
      get :average_mrs_per_milestone
      get :throughputs
      get :average_mrs_per_month
      get :monthly_merged_mrs
      get :time_to_close_bugs
      resources :teams, only: [:show, :index], constraints: { id: /[^\/]+/ }
      resources :stages, only: [:show, :index]
      resources :sections, only: [:show]
    end

    get :bugs_by_team, to: redirect { |path_params, req| "/groups/#{path_params[:group_id]}/bugs_by_stage" }
    get :bugs_by_stage
    get :bugs_by_severity
    get :bugs_by_priority
    get :bugs_past_slo
    get :feature_proposals_by_team
    get :issues_by_state
    get :mrs_by_state
    get :regressions
    get :completed_deliverables_per_milestone
    get :missed_deliverables
    get :bug_classification
    get :developer_productivity_metrics, to: redirect { |path_params, req| "/groups/#{path_params[:group_id]}/average_mrs_per_milestone" }
    get :average_mrs_per_milestone
    get :throughputs
    get :average_mrs_per_month
    get :monthly_merged_mrs
    get :time_to_close_bugs
    resources :teams, only: [:show, :index]
    resources :stages, only: [:show, :index]
    resources :sections, only: [:show]
  end

  root 'groups#index'

  if Rails.env.development?
    mount GraphiQL::Rails::Engine, at: "/graphiql", graphql_path: "/graphql"
  end

  post "/graphql", to: "graphql#execute", as: "graphql"
  post "/download/raw.:format", to: "raw_data#download", as: "download_raw_data"
  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  # root 'welcome#index'

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
