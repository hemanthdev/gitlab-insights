class RemoveGitRefColumnsFromMergeRequests < ActiveRecord::Migration
  def change
    remove_column(:merge_requests, :source_branch, :string)
    remove_column(:merge_requests, :target_branch, :string)
    remove_column(:merge_requests, :sha, :string)
    remove_column(:merge_requests, :merge_commit_sha, :string)
  end
end
