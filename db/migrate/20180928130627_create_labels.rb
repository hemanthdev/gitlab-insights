class CreateLabels < ActiveRecord::Migration
  def change
    create_table :labels do |t|
      t.integer :labels_id
      t.string :name
      t.string :color
      t.string :description
      t.integer :open_issues_count
      t.integer :closed_issues_count
      t.integer :open_merge_requests_count
      t.integer :priority
      t.boolean :subscribed
    end

    add_reference :labels, :project, foreign_key: true
  end
end
