class CreateProjects < ActiveRecord::Migration
  def change
    create_table :projects do |t|
      t.string :path
    end

    add_reference :projects, :group, foreign_key: true
  end
end
