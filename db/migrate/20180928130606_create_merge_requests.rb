class CreateMergeRequests < ActiveRecord::Migration
  def change
    create_table :merge_requests do |t|
      t.integer :merge_requests_id
      t.integer :iid
      t.string :state
      t.datetime :created_at
      t.datetime :updated_at
      t.datetime :closed_at
      t.string :target_branch
      t.string :source_branch
      t.integer :upvotes
      t.integer :downvotes
      t.string :labels, array: true, default: []
      t.boolean :work_in_progress
      t.json :milestone
      t.boolean :merge_when_pipeline_succeeds
      t.string :merge_status
      t.string :sha
      t.string :merge_commit_sha
      t.integer :user_notes_count
      t.boolean :discussion_locked
      t.boolean :should_remove_source_branch
      t.boolean :force_remove_source_branch
      t.json :time_stats
      t.integer :approvals_before_merge
      t.boolean :squash
      t.boolean :allow_maintainer_to_push
    end

    add_reference :merge_requests, :project, foreign_key: true
  end
end
