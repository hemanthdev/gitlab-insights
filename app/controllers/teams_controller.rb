class TeamsController < ProjectsController
  before_action :find_project
  before_action :respond_404_for_dev_projects

  def show
    @source = @project || @group
    @team = params[:id].titleize.sub('Deprecated', 'DEPRECATED')
  end
end
