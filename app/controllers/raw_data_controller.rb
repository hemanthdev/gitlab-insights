# frozen_string_literal: true

require 'csv'

class RawDataController < ApplicationController
  protect_from_forgery with: :null_session

  def download
    respond_to do |format|
      format.csv do
        send_data csv_data, filename: "#{params[:name]}.csv"
      end
    end
  end

  private

  def csv_data
    issuables_raw_data =
      raw_data.dig('data', 'group', 'issuables_raw_data') ||
        raw_data.dig('data', 'project', 'issuables_raw_data')

    CSV.generate do |csv|
      csv << all_fields

      issuables_raw_data.each do |raw_data|
        csv << raw_data.values
      end
    end
  end

  def raw_data
    @raw_data ||= GitlabInsightsDashboardSchema.execute(query).to_h
  end

  def all_fields
    @all_fields ||= Types::IssuableType.fields.keys
  end

  def query
    params[:query].sub(
      # FIXME: We should not remove {} so greedily!
      /#{Regexp.escape(params[:name])}(.+?)\s*\{.+\}/,
      "issuables_raw_data\\1 { #{all_fields.join(', ')} } }}")
  end
end
