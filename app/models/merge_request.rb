class MergeRequest < ActiveRecord::Base
  include Issuable

  belongs_to :project

  def issuable_id
    merge_requests_id
  end

  def confidential
  end
end
