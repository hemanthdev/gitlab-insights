# frozen_string_literal: true

module Issuable
  THROUGHPUT_LABELS = %w[
    Community\ contribution
    security
    bug
    feature
    backstage
  ].freeze

  def milestone_title
    milestone&.dig('title')
  end

  def author_username
    author&.dig('username')
  end

  def project_full_path
    project.full_path
  end

  def url
    "https://gitlab.com/#{project_full_path}/#{self.class.name.underscore.pluralize}/#{iid}"
  end

  def throughput
    (THROUGHPUT_LABELS & labels).first
  end
end
