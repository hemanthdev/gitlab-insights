IssuesOpenedClosedPerMonthChart = (element) ->
  query = @queries.ScopedQuery(@queries.IssuablesOpenedClosedPerMonthQuery(
    @query_strings.IssuesScope
  ))
  @charts.LineChart(element, query.query_string, query.view_string)

BugsOpenedClosedPerMonthChart = (element) ->
  query = @queries.ScopedQuery(@queries.IssuablesOpenedClosedPerMonthQueryFiltered(
    @query_strings.IssuesScope,
    @query_strings.Bugs
  ))
  @charts.LineChart(element, query.query_string, query.view_string)

$(document).ready () ->
  if document.getElementById('issues-opened-closed-month')
    IssuesOpenedClosedPerMonthChart(document.getElementById('issues-opened-closed-month'))
  if document.getElementById('bugs-opened-closed-month')
    BugsOpenedClosedPerMonthChart(document.getElementById('bugs-opened-closed-month'))
