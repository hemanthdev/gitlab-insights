TimeToCloseBugsChart = (element) ->
  query = @queries.ScopedQuery(@queries.TimeToCloseIssues(
    'Closed',
    '["bug"]',
    @query_strings.TeamMRMonths
  ))
  @charts.ComboBarLineChart(element, query.query_string, query.view_string)

TimeToCloseS1BugsChart = (element) ->
  query = @queries.ScopedQuery(@queries.TimeToCloseIssues(
    'Closed',
    '["bug", "S1"]',
    @query_strings.TeamMRMonths
  ))
  @charts.ComboBarLineChart(element, query.query_string, query.view_string)

TimeToCloseS2BugsChart = (element) ->
  query = @queries.ScopedQuery(@queries.TimeToCloseIssues(
    'Closed',
    '["bug", "S2"]',
    @query_strings.TeamMRMonths
  ))
  @charts.ComboBarLineChart(element, query.query_string, query.view_string)

TimeToCloseS1CustomerBugsChart = (element) ->
  query = @queries.ScopedQuery(@queries.TimeToCloseIssues(
    'Closed',
    '["bug", "S1", "customer"]',
    @query_strings.TeamMRMonths
  ))
  @charts.ComboBarLineChart(element, query.query_string, query.view_string)

TimeToCloseS2CustomerBugsChart = (element) ->
  query = @queries.ScopedQuery(@queries.TimeToCloseIssues(
    'Closed',
    '["bug", "S2", "customer"]',
    @query_strings.TeamMRMonths
  ))
  @charts.ComboBarLineChart(element, query.query_string, query.view_string)

$(document).ready () ->
  if document.getElementById('time-to-close-bugs')
    TimeToCloseBugsChart(document.getElementById('time-to-close-bugs'))
  if document.getElementById('time-to-close-bugs-s1')
    TimeToCloseS1BugsChart(document.getElementById('time-to-close-bugs-s1'))
  if document.getElementById('time-to-close-bugs-s2')
    TimeToCloseS2BugsChart(document.getElementById('time-to-close-bugs-s2'))
  if document.getElementById('time-to-close-bugs-s1-customer')
    TimeToCloseS1CustomerBugsChart(document.getElementById('time-to-close-bugs-s1-customer'))
  if document.getElementById('time-to-close-bugs-s2-customer')
    TimeToCloseS2CustomerBugsChart(document.getElementById('time-to-close-bugs-s2-customer'))
