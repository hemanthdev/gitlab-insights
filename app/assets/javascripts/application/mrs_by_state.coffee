MonthlyMergeRequestsPerStateChart = (element) ->
  query = @queries.ScopedQuery(@queries.MonthlyMergeRequestsPerStateQuery(
    @query_strings.MergeRequestsScope
  ))
  @charts.LineChart(element, query.query_string, query.view_string)

$(document).ready () ->
  if document.getElementById('monthly-merge-requests-per-state')
    MonthlyMergeRequestsPerStateChart(document.getElementById('monthly-merge-requests-per-state'))
