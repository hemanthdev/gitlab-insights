RegressionsPerMilestoneChart = (element) ->
  query = @queries.ScopedQuery(@queries.RegressionsQuery(
    'Open',
    @query_strings.Regression,
    @query_strings.RegressionMilestones,
    @query_strings.IssuesScope
  ))
  @charts.BarChart(element, query.query_string, query.view_string)

$(document).ready () ->
  if document.getElementById('regressions-per-milestone')
    RegressionsPerMilestoneChart(document.getElementById('regressions-per-milestone'))
