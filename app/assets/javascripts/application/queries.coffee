group_query = (group_path, view_string, inner_query) ->
  view_string: view_string,
  query_string: '
  {
    group(group_path:"' + group_path + '") {' +
      inner_query + '
    }
  }
  '
project_query = (group_path, project_path, view_string, inner_query) ->
  view_string: view_string,
  query_string: '
  {
    project(group_path:"' + group_path + '", project_path:"' + project_path + '") {' +
      inner_query + '
    }
  }
  '

@queries =
  ScopedQuery: (inner_query) ->
    group_path = $('#group-path').val()
    project_path = $('#project-path').val()

    if project_path
      project_query(group_path, project_path, inner_query['view_string'], inner_query['query_string'])
    else
      group_query(group_path, inner_query['view_string'], inner_query['query_string'])
  CumulativeIssuablesCreatedPerMonthQuery: (state_string, filter_labels_string, collection_labels_string, issuable_scope) ->
    view_string: 'cumulative_issuables_created_per_month',
    query_string: '
    cumulative_issuables_created_per_month(state:' + state_string + ', filter_labels:' + filter_labels_string + ', collection_labels: ' + collection_labels_string + ', issuable_scope: ' + issuable_scope + ') {
      labels
      datasets {
        label
        data
        borderColor
        backgroundColor
      }
    }
    '
  IssuablesCreatedPerMonthQuery: (state_string, filter_labels_string, collection_labels_string, issuable_scope) ->
    view_string: 'issuables_created_per_month',
    query_string: '
    issuables_created_per_month(state:' + state_string + ', filter_labels:' + filter_labels_string + ', collection_labels: ' + collection_labels_string + ', issuable_scope: ' + issuable_scope + ') {
      labels
      datasets {
        label
        data
        borderColor
        backgroundColor
      }
    }
    '
  IssuablesClosedPerMonthQuery: (state_string, filter_labels_string, collection_labels_string, issuable_scope) ->
    view_string: 'issuables_closed_per_month',
    query_string: '
    issuables_closed_per_month(state:' + state_string + ', filter_labels:' + filter_labels_string + ', collection_labels: ' + collection_labels_string + ', issuable_scope: ' + issuable_scope + ') {
      labels
      datasets {
        label
        data
        borderColor
        backgroundColor
      }
    }
    '
  IssuablesMergedPerMonthQuery: (state_string, filter_labels_string, collection_labels_string, issuable_scope, period_limit) ->
    view_string: 'issuables_merged_per_month',
    query_string: '
    issuables_merged_per_month(state:' + state_string + ', filter_labels:' + filter_labels_string + ', collection_labels: ' + collection_labels_string + ', issuable_scope: ' + issuable_scope + ', period_limit: ' + period_limit + ') {
      labels
      datasets {
        label
        data
        backgroundColor
      }
    }
    '
  IssuablesMergedPerWeekQuery: (state_string, filter_labels_string, collection_labels_string, issuable_scope, period_limit) ->
    view_string: 'issuables_merged_per_week',
    query_string: '
    issuables_merged_per_week(state:' + state_string + ', filter_labels:' + filter_labels_string + ', collection_labels: ' + collection_labels_string + ', issuable_scope: ' + issuable_scope + ', period_limit: ' + period_limit + ') {
      labels
      datasets {
        label
        data
        backgroundColor
      }
    }
    '
  IssuablesCreatedPerDayQuery: (state_string, filter_labels_string, collection_labels_string, issuable_scope) ->
    view_string: 'issuables_created_per_day',
    query_string: '
    issuables_created_per_day(state:' + state_string + ', filter_labels:' + filter_labels_string + ', collection_labels: ' + collection_labels_string + ', issuable_scope: ' + issuable_scope + ') {
      labels
      datasets {
        label
        data
        backgroundColor
      }
    }
    '
  IssuablesClosedPerDayQuery: (state_string, filter_labels_string, collection_labels_string, issuable_scope) ->
    view_string: 'issuables_closed_per_day',
    query_string: '
    issuables_closed_per_day(state:' + state_string + ', filter_labels:' + filter_labels_string + ', collection_labels: ' + collection_labels_string + ', issuable_scope: ' + issuable_scope + ') {
      labels
      datasets {
        label
        data
        backgroundColor
      }
    }
    '
  IssuablesOpenedClosedPerMonthQuery: (issuable_scope) ->
    view_string: 'monthly_issuables_per_state',
    query_string: '
    monthly_issuables_per_state(issuable_scope: ' + issuable_scope + ') {
      labels
      datasets {
        label
        data
        fill
        borderColor
      }
    }
    '
  IssuablesOpenedClosedPerMonthQueryFiltered: (issuable_scope, filter_labels_string) ->
    view_string: 'monthly_issuables_per_state',
    query_string: '
    monthly_issuables_per_state(issuable_scope: ' + issuable_scope + ', filter_labels:' + filter_labels_string + ') {
      labels
      datasets {
        label
        data
        fill
        borderColor
      }
    }
    '
  MonthlyMergeRequestsPerStateQuery: (issuable_scope) ->
    view_string: 'monthly_merge_requests_per_state',
    query_string: '
    monthly_merge_requests_per_state(issuable_scope: ' + issuable_scope + ') {
      labels
      datasets {
        label
        data
        fill
        borderColor
      }
    }
    '
  IssuablesPerLabelQuery: (state_string, filter_labels_string, collection_labels_string, issuable_scope) ->
    view_string: 'issuables_per_label',
    query_string: '
    issuables_per_label(state:' + state_string + ', filter_labels:' + filter_labels_string + ', collection_labels: ' + collection_labels_string + ', issuable_scope: ' + issuable_scope + ') {
      labels
      datasets {
        data
        backgroundColor
      }
    }
    '
  IssuablesPerLabelPercentageQuery: (state_string, filter_labels_string, collection_labels_string, issuable_scope) ->
    view_string: 'issuables_per_label_percentage',
    query_string: '
    issuables_per_label_percentage(state:' + state_string + ', filter_labels:' + filter_labels_string + ', collection_labels: ' + collection_labels_string + ', issuable_scope: ' + issuable_scope + ') {
      data
      backgroundColor
    }
    '
  RegressionsQuery: (state_string, filter_labels_string, collection_labels_string, issuable_scope) ->
    view_string: 'regressions',
    query_string: '
    regressions(state:' + state_string + ', filter_labels:' + filter_labels_string + ', collection_labels: ' + collection_labels_string + ', issuable_scope: ' + issuable_scope + ') {
      labels
      datasets {
        data
        backgroundColor
      }
    }
    '
  IssuablesPerMilestoneQuery: (issuable_scope, state_string, filter_labels_string, collection_labels_string) ->
    view_string: 'issuables_per_milestone',
    query_string: '
    issuables_per_milestone(issuable_scope: ' + issuable_scope + ', state:' + state_string + ', filter_labels:' + filter_labels_string + ', collection_labels:' + collection_labels_string + ') {
      labels
      datasets {
        label
        data
        backgroundColor
      }
    }
    '
  MissedDeliverablesQuery: (state_string, filter_labels_string, collection_labels_string, issuable_scope) ->
    view_string: 'missed_deliverables',
    query_string: '
    missed_deliverables(state:' + state_string + ', filter_labels:' + filter_labels_string + ', collection_labels: ' + collection_labels_string + ', issuable_scope: ' + issuable_scope + ', multi_bucket: true) {
      labels
      datasets {
        data
        backgroundColor
      }
    }
    '
  AverageIssuablesPerMilestone: (state_string, issuable_scope, exclude_community_contributions) ->
    view_string: 'average_issuables_per_milestone',
    query_string: '
    average_issuables_per_milestone(state:' + state_string + ', issuable_scope: ' + issuable_scope + ', exclude_community_contributions: ' + exclude_community_contributions + ') {
      labels
      line_dataset {
        type
        label
        data
        borderColor
        borderWidth
      }
      bar_dataset {
        type
        label
        data
        backgroundColor
        borderColor
        borderWidth
      }
    }
    '
  MergedMergeRequestsScopedByLabelsQuery: (period, collection_labels_string, period_limit, exclude_community_contributions = false) ->
    view_string: 'issuables_merged_per_' + period,
    query_string: '
    issuables_merged_per_' + period + '(issuable_scope: MergeRequests, state:Merged, collection_labels: ' + collection_labels_string + ', period_limit: ' + period_limit + ', exclude_community_contributions: ' + exclude_community_contributions + ') {
      labels
      datasets {
        label
        data
        backgroundColor
      }
    }
    '
  MergedMRsPerMonthWithAverageQuery: (period_limit, exclude_community_contributions = false) ->
    view_string: 'monthly_merged_mrs',
    query_string: '
    monthly_merged_mrs(state:Merged, issuable_scope: MergeRequests, period_limit: ' + period_limit + ', exclude_community_contributions: ' + exclude_community_contributions + ') {
      labels
      line_dataset {
        type
        label
        data
        borderColor
        borderWidth
      }
      bar_dataset {
        type
        label
        data
        backgroundColor
        borderColor
        borderWidth
      }
    }
    '
  AverageIssuablesMergedPerMonth: (state_string, issuable_scope, exclude_community_contributions) ->
    view_string: 'average_issuables_merged_per_month',
    query_string: '
    average_issuables_merged_per_month(state:' + state_string + ', issuable_scope: ' + issuable_scope + ', exclude_community_contributions: ' + exclude_community_contributions + ') {
      labels
      line_dataset {
        type
        label
        data
        borderColor
        borderWidth
      }
      bar_dataset {
        type
        label
        data
        backgroundColor
        borderColor
        borderWidth
      }
    }
    '
  TimeToCloseIssues: (state_string, filter_labels, period_limit) ->
    view_string: 'time_to_close_issues',
    query_string: '
    time_to_close_issues(state:' + state_string + ', issuable_scope: Issues, filter_labels: ' + filter_labels + ', period_limit: ' + period_limit + ') {
      labels
      line_dataset {
        type
        label
        data
        borderColor
        borderWidth
      }
      bar_dataset {
        type
        label
        data
        backgroundColor
        borderColor
        borderWidth
      }
    }
    '
