ThroughputsPerWeekChart = (element) ->
  query = @queries.ScopedQuery(@queries.MergedMergeRequestsScopedByLabelsQuery(
    'week',
    @query_strings.MergeRequestCategories,
    @query_strings.TeamMRWeeks
  ))
  @charts.StackedBarChart(element, query.query_string, query.view_string)
ThroughputsPerMonthChart = (element) ->
  query = @queries.ScopedQuery(@queries.MergedMergeRequestsScopedByLabelsQuery(
    'month',
    @query_strings.MergeRequestCategories,
    @query_strings.TeamMRMonths
  ))
  @charts.StackedBarChart(element, query.query_string, query.view_string)

$(document).ready () ->
  if document.getElementById('throughputs-per-week')
    ThroughputsPerWeekChart(document.getElementById('throughputs-per-week'))
  if document.getElementById('throughputs-per-month')
    ThroughputsPerMonthChart(document.getElementById('throughputs-per-month'))
