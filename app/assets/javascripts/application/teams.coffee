team_label_query_segment = (label_json) ->
  label_array = if label_json then JSON.parse(label_json) else []
  label_array.push($('#team-label').val())
  JSON.stringify(label_array)
TeamBugsSeverityNumber = (element) ->
  query = @queries.ScopedQuery(@queries.IssuablesPerLabelPercentageQuery(
    'Open',
    team_label_query_segment(@query_strings.Bugs),
    @query_strings.Severities,
    @query_strings.IssuesScope
  ))
  @charts.BigNumberPercentage(element, query.query_string, query.view_string)
TeamBugsPriorityNumber = (element) ->
  query = @queries.ScopedQuery(@queries.IssuablesPerLabelPercentageQuery(
    'Open',
    team_label_query_segment(@query_strings.Bugs),
    @query_strings.Priorities,
    @query_strings.IssuesScope
  ))
  @charts.BigNumberPercentage(element, query.query_string, query.view_string)
TeamBugsSeverityPie = (element) ->
  query = @queries.ScopedQuery(@queries.IssuablesPerLabelQuery(
    'Open',
    team_label_query_segment(@query_strings.Bugs),
    @query_strings.Severities,
    @query_strings.IssuesScope
  ))
  @charts.PieChart(element, query.query_string, query.view_string)
TeamBugsPriorityPie = (element) ->
  query = @queries.ScopedQuery(@queries.IssuablesPerLabelQuery(
    'Open',
    team_label_query_segment(@query_strings.Bugs),
    @query_strings.Priorities,
    @query_strings.IssuesScope
  ))
  @charts.PieChart(element, query.query_string, query.view_string)
TeamRegressionsPerMilestoneChart = (element) ->
  query = @queries.ScopedQuery(@queries.RegressionsQuery(
    'Open',
    team_label_query_segment(@query_strings.Regression),
    @query_strings.RegressionMilestones,
    @query_strings.IssuesScope
  ))
  @charts.BarChart(element, query.query_string, query.view_string)
TeamCompletedDeliverablesPerMilestoneChart = (element) ->
  query = @queries.ScopedQuery(@queries.IssuablesPerMilestoneQuery(
    @query_strings.IssuesScope
    'Closed',
    team_label_query_segment(null),
    @query_strings.Deliverable
  ))
  @charts.StackedBarChart(element, query.query_string, query.view_string)
TeamMissedDeliverablesPerMilestoneChart = (element) ->
  query = @queries.ScopedQuery(@queries.MissedDeliverablesQuery(
    'Open',
    team_label_query_segment(@query_strings.MissedDeliverable),
    @query_strings.MissedDeliverableMilestones,
    @query_strings.IssuesScope
  ))
  @charts.BarChart(element, query.query_string, query.view_string)
MRsMergedPerWeekChart = (element) ->
  query = @queries.ScopedQuery(@queries.IssuablesMergedPerWeekQuery(
    'Merged',
    team_label_query_segment(null),
    @query_strings.MergeRequestCategories,
    @query_strings.MergeRequestsScope,
    @query_strings.TeamMRWeeks
  ))
  @charts.StackedBarChart(element, query.query_string, query.view_string)
MRsMergedPerMonthChart = (element) ->
  query = @queries.ScopedQuery(@queries.IssuablesMergedPerMonthQuery(
    'Merged',
    team_label_query_segment(null),
    @query_strings.MergeRequestCategories,
    @query_strings.MergeRequestsScope,
    @query_strings.TeamMRMonths
  ))
  @charts.StackedBarChart(element, query.query_string, query.view_string)
BugsCreatedPerMonthBarChart = (element) ->
  query = @queries.ScopedQuery(@queries.IssuablesCreatedPerMonthQuery(
    'Open',
    team_label_query_segment(@query_strings.Bugs),
    @query_strings.Priorities,
    @query_strings.IssuesScope
  ))
  @charts.StackedBarChart(element, query.query_string, query.view_string)
BugsClosedPerMonthBarChart = (element) ->
  query = @queries.ScopedQuery(@queries.IssuablesClosedPerMonthQuery(
    'Closed',
    team_label_query_segment(@query_strings.Bugs),
    @query_strings.Priorities,
    @query_strings.IssuesScope
  ))
  @charts.StackedBarChart(element, query.query_string, query.view_string)
BugsOpenedClosedPerMonthChartTeam = (element) ->
  query = @queries.ScopedQuery(@queries.IssuablesOpenedClosedPerMonthQueryFiltered(
    @query_strings.IssuesScope,
    team_label_query_segment(@query_strings.Bugs)
  ))
  @charts.LineChart(element, query.query_string, query.view_string)
CumulativeBugsCreatedByPriorityPerMonthChartTeam = (element) ->
  query = @queries.ScopedQuery(@queries.CumulativeIssuablesCreatedPerMonthQuery(
    'Open',
    team_label_query_segment(@query_strings.Bugs),
    @query_strings.Priorities,
    @query_strings.IssuesScope
  ))
  @charts.StackedBarChart(element, query.query_string, query.view_string)


$(document).ready () ->
  if document.getElementById('team-bugs-severity-number')
    TeamBugsSeverityNumber(document.getElementById('team-bugs-severity-number'))
  if document.getElementById('team-bugs-priority-number')
    TeamBugsPriorityNumber(document.getElementById('team-bugs-priority-number'))
  if document.getElementById('team-bugs-severity-pie')
    TeamBugsSeverityPie(document.getElementById('team-bugs-severity-pie'))
  if document.getElementById('team-bugs-priority-pie')
    TeamBugsPriorityPie(document.getElementById('team-bugs-priority-pie'))
  if document.getElementById('team-regressions-per-milestone')
    TeamRegressionsPerMilestoneChart(document.getElementById('team-regressions-per-milestone'))
  if document.getElementById('team-completed-deliverables-per-milestone')
    TeamCompletedDeliverablesPerMilestoneChart(document.getElementById('team-completed-deliverables-per-milestone'))
  if document.getElementById('team-missed-deliverables-per-milestone')
    TeamMissedDeliverablesPerMilestoneChart(document.getElementById('team-missed-deliverables-per-milestone'))
  if document.getElementById('team-mrs-per-week')
    MRsMergedPerWeekChart(document.getElementById('team-mrs-per-week'))
  if document.getElementById('team-mrs-per-month')
    MRsMergedPerMonthChart(document.getElementById('team-mrs-per-month'))
  if document.getElementById('bugs-created-per-month-bar')
    BugsCreatedPerMonthBarChart(document.getElementById('bugs-created-per-month-bar'))
  if document.getElementById('bugs-closed-per-month-bar')
    BugsClosedPerMonthBarChart(document.getElementById('bugs-closed-per-month-bar'))
  if document.getElementById('bugs-opened-closed-month-team')
    BugsOpenedClosedPerMonthChartTeam(document.getElementById('bugs-opened-closed-month-team'))
  if document.getElementById('cumulative-bugs-created-priority-month-team')
    CumulativeBugsCreatedByPriorityPerMonthChartTeam(document.getElementById('cumulative-bugs-created-priority-month-team'))
