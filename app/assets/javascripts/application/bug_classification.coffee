BugsSeverityNumber = (element) ->
  query = @queries.ScopedQuery(@queries.IssuablesPerLabelPercentageQuery(
    'Open',
    @query_strings.Bugs,
    @query_strings.Severities,
    @query_strings.IssuesScope
  ))
  @charts.BigNumberPercentage(element, query.query_string, query.view_string)

BugsPriorityNumber = (element) ->
  query = @queries.ScopedQuery(@queries.IssuablesPerLabelPercentageQuery(
    'Open',
    @query_strings.Bugs,
    @query_strings.Priorities,
    @query_strings.IssuesScope
  ))
  @charts.BigNumberPercentage(element, query.query_string, query.view_string)

BugsSeverityPie = (element) ->
  query = @queries.ScopedQuery(@queries.IssuablesPerLabelQuery(
    'Open',
    @query_strings.Bugs,
    @query_strings.Severities,
    @query_strings.IssuesScope
  ))
  @charts.PieChart(element, query.query_string, query.view_string)

BugsPriorityPie = (element) ->
  query = @queries.ScopedQuery(@queries.IssuablesPerLabelQuery(
    'Open',
    @query_strings.Bugs,
    @query_strings.Priorities,
    @query_strings.IssuesScope
  ))
  @charts.PieChart(element, query.query_string, query.view_string)

$(document).ready () ->
  if document.getElementById('bugs-severity-number')
    BugsSeverityNumber(document.getElementById('bugs-severity-number'))
  if document.getElementById('bugs-priority-number')
    BugsPriorityNumber(document.getElementById('bugs-priority-number'))
  if document.getElementById('bugs-severity-pie')
    BugsSeverityPie(document.getElementById('bugs-severity-pie'))
  if document.getElementById('bugs-priority-pie')
    BugsPriorityPie(document.getElementById('bugs-priority-pie'))
