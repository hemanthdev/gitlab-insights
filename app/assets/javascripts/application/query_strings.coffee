@query_strings =
  IssuesScope: 'Issues',
  MergeRequestsScope: 'MergeRequests',
  Teams: '["Manage [DEPRECATED]", "Plan [DEPRECATED]", "Create [DEPRECATED]", "Package [DEPRECATED]", "Serverless [DEPRECATED]", "Fulfillment [DEPRECATED]", "Release [DEPRECATED]", "Verify [DEPRECATED]", "Configure [DEPRECATED]", "Monitor [DEPRECATED]", "Secure [DEPRECATED]", "Distribution [DEPRECATED]", "Gitaly [DEPRECATED]", "Geo [DEPRECATED]", "Quality", "security", "Gitter [DEPRECATED]", "frontend"]',
  Stages: '["devops::manage", "devops::plan", "devops::create", "devops::verify", "devops::package", "devops::release", "devops::configure", "devops::monitor", "devops::secure", "devops::defend", "devops::growth", "devops::enablement"]',
  Severities: '["S4", "S3", "S2", "S1"]',
  Priorities: '["P4", "P3", "P2", "P1"]',
  Bugs: '["bug"]',
  BugsPastSlo: '["bug", "missed-SLO"]',
  CustomerBugsPastSlo: '["bug", "customer", "missed-SLO"]',
  FeatureProposals: '["feature"]',
  Regression: '["regression"]',
  RegressionMilestones: '["regression:11.0","regression:11.1","regression:11.2","regression:11.3","regression:11.4","regression:11.5", "regression:11.6", "regression:11.7", "regression:11.8", "regression:11.9", "regression:11.10", "regression:11.11", "regression:12.0", "regression:12.1", "regression:12.2", "regression:12.3", "regression:12.4", "regression:12.5"]',
  Deliverable: '["Deliverable"]',
  MissedDeliverable: '["missed-deliverable"]',
  MissedDeliverableMilestones: '["missed:11.0","missed:11.1","missed:11.2","missed:11.3","missed:11.4","missed:11.5", "missed:11.6", "missed:11.7", "missed:11.8", "missed:11.9", "missed:11.10", "missed:11.11", "missed:12.0", "missed:12.1", "missed:12.2", "missed:12.3", "missed:12.4", "missed:12.5"]'
  MergeRequestCategories: '["Community contribution","security","bug","feature","backstage"]'
  TeamMRWeeks: 12
  TeamMRMonths: 24
