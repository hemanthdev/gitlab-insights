Types::BarChartType = GraphQL::ObjectType.define do
  name "BarChartType"

  field :labels, types[!types.String] do
    resolve ->(obj, arg, ctx) { obj[ctx.key] }
  end
  field :datasets, types[!Types::BarChartDatasetType] do
    resolve ->(obj, arg, ctx) { obj[ctx.key] }
  end
end
