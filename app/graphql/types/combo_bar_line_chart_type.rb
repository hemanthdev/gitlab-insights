Types::ComboBarLineChartType = GraphQL::ObjectType.define do
  name "ComboBarLineChartType"

  field :labels, types[!types.String] do
    resolve ->(obj, arg, ctx) { obj[ctx.key] }
  end
  field :line_dataset, types[!Types::ComboBarLineChartLineDatasetType] do
    resolve ->(obj, arg, ctx) { obj[ctx.key] }
  end
  field :bar_dataset, types[!Types::ComboBarLineChartBarDatasetType] do
    resolve ->(obj, arg, ctx) { obj[ctx.key] }
  end
end
