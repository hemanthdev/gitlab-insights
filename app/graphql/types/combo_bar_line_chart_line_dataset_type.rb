require 'digest/md5'

Types::ComboBarLineChartLineDatasetType = GraphQL::ObjectType.define do
  name "ComboBarLineChartLineDatasetType"

  field :type, !types.String do
    resolve ->(obj, arg, ctx) { obj[ctx.key] }
  end
  field :label, !types.String do
    resolve ->(obj, arg, ctx) { obj[ctx.key] }
  end
  field :borderColor, !types.String do
    resolve ->(obj, arg, ctx) { obj[ctx.key] }
  end
  field :borderWidth, types.Int do
    resolve ->(obj, arg, ctx) { obj[ctx.key] }
  end
  # field :backgroundColor, types[!types.String] do
  #   resolve ->(obj, arg, ctx) { obj[ctx.key] }
  # end
  field :data, types[types.Float] do
    resolve ->(obj, arg, ctx) { obj[ctx.key] }
  end
end
