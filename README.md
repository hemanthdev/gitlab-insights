# GitLab insights

**Note**: This project is **DEPRECATED** and no longer in active development in favor of migrating our metrics into our Insights a native GitLab feature available at [gitlab-org Insights Charts](https://gitlab.com/groups/gitlab-org/-/insights).
High level management metrics will be migrated to our [Periscope dashboards](https://app.periscopedata.com/app/gitlab/).

Please follow along in the deprecation plan issue: [Deprecation plan for gitlab-insights and transition to GitLab (native) Insights or Periscope](https://gitlab.com/gitlab-org/gitlab-insights/issues/116).

- [Docker config](docs/docker.md)
- [Postgres](docs/database.md)
- [Data Sync](docs/sync.md)
- [Adding/Checking Data](docs/adding_data.md)
- [Dedicated runner](docs/dedicated_runner.md)

## Local setup

### Prerequisites

- PostgreSQL

### Prep

```
git clone https://gitlab.com/gitlab-org/gitlab-insights
gem install bundler
bundle install --path vendor/bundle
bundle exec rake db:setup
```

### Add a group and a project

Start the rails console:

```
bundle exec rails c
```

Create a group and a project:

```ruby
g = Group.create(path: 'gitlab-org');
g.projects.create(path: 'gitlab');
```

For more info see [adding data pages](docs/adding_data.md)

### Populate with some data

1. Create an access token on GitLab.com to use for retrieving data

Run the limited populate step to add some data:

```
bundle exec rake "gitlab_insights:populate_limited[personal_access_token]" RAILS_ENV=development
```

A full populate will take considerably longer depending on the number of issues and merge requests in the target project:

```
bundle exec rake "gitlab_insights:populate[personal_access_token]" RAILS_ENV=development
```

If adding projects for `dev` include a second access token for that instance:


```
bundle exec rake "gitlab_insights:populate[personal_access_token, dev_access_token]" RAILS_ENV=development
```

### Start the app

```
bundle exec rails s
```
