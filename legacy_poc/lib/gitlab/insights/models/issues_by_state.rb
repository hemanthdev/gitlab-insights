require 'active_record'

module Gitlab
  module Insights
    module Models
      class IssuesByState < ActiveRecord::Base
        enum state: {
          opened: 0,
          closed: 1
        }

        def self.schema!
          connection.create_table(table_name) do |t|
            t.datetime  :date
            t.integer   :state
            t.string    :count
            t.timestamps null: true
          end
        end
      end
    end
  end
end
