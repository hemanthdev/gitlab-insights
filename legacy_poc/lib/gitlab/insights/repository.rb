require 'active_support/all'

require_relative 'models/issues_by_state'

module Gitlab
  module Insights
    class Repository
      attr_reader :adapter, :options

      def initialize(adapter, options = {})
        @adapter = adapter
        @options = options
      end

      def self.install
        new.install
      end

      def self.uninstall
        new.uninstall
      end

      def self.reset
        new.reset
      end

      def install
        Models::IssuesByState.schema! unless prepared?(Models::IssuesByState)
      end

      def uninstall
        connection.drop_table(Models::IssuesByState.table_name)
      end

      def reset
        Models::IssuesByState.destroy_all
      end

      def insert(model, attributes)
        model.create!(attributes.except(:created_at))
      end

      private

      def prepared?(model)
        connection.table_exists?(model.table_name)
      end

      def connection
        @connection ||= establish_connection
      end

      def establish_connection
        adapter.establish_connection.connection
      end
    end
  end
end
