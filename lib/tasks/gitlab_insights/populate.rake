namespace :gitlab_insights do
  DEFAULT_API_ENDPOINT = 'https://gitlab.com/api/v4'
  DEV_API_ENDPOINT = 'https://dev.gitlab.org/api/v4'
  LIMIT = 500

  desc 'Clear resources and populate the DB with data for all projects'
  task :populate, [:token, :dev_token] => :environment do |_, args|
    args.with_defaults(dev_token: nil)

    GitlabInsights::DbPopulator.new(client(args[:token]), dev_client(args[:dev_token])).execute!
  end

  desc 'Clear resources and populate the DB with data for all projects'
  task :populate_limited, [:token, :dev_token] => :environment do |_, args|
    args.with_defaults(dev_token: nil)

    GitlabInsights::DbPopulator.new(client(args[:token]), dev_client(args[:dev_token])).execute!(LIMIT)
  end

  def client(token)
    GitlabInsights::Api::Client.new(DEFAULT_API_ENDPOINT, token)
  end

  def dev_client(token)
    if token
      GitlabInsights::Api::Client.new(DEV_API_ENDPOINT, token)
    end
  end
end
