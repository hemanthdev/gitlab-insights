module GitlabInsights
  COLOR_SCHEME = {
    red: '#e6194B',
    green: '#3cb44b',
    yellow: '#ffe119',
    blue: '#4363d8',
    orange: '#f58231',
    purple: '#911eb4',
    cyan: '#42d4f4',
    magenta: '#f032e6',
    lime: '#bfef45',
    pink: '#fabebe',
    teal: '#469990',
    lavender: '#e6beff',
    brown: '#9A6324',
    beige: '#fffac8',
    maroon: '#800000',
    mint: '#aaffc3',
    olive: '#808000',
    apricot: '#ffd8b1',
    black: '#000000'
  }

  UNCATEGORIZED = 'undefined'
  UNCATEGORIZED_COLOR = "#808080"
  TOP_COLOR = "#FF0000"
  HIGH_COLOR = "#ff8800"
  MEDIUM_COLOR = "#fff600"
  LOW_COLOR = "#008000"
  PROPOSAL_COLOR = "#f0ad4e"
  BUG_COLOR = "#ff0000"
  SECURITY_COLOR = "#d9534f"
  COMMUNITY_CONTRIBUTION_COLOR = "#a8d695"
  DEFAULT_COLOR = "#428bca"
  LINE_COLOR = COLOR_SCHEME[:red]

  AVG_COLOR = "#db4437"
  PERCENTILE_85_COLOR = "#f4b400"
  PERCENTILE_95_COLOR = "#3c78d8"
  CLOSED_ISSUES_COLOR = COLOR_SCHEME[:black]

  STATIC_COLOR_MAP = {
    UNCATEGORIZED => UNCATEGORIZED_COLOR,
    "S1" => TOP_COLOR,
    "S2" => HIGH_COLOR,
    "S3" => MEDIUM_COLOR,
    "S4" => LOW_COLOR,
    "P1" => TOP_COLOR,
    "P2" => HIGH_COLOR,
    "P3" => MEDIUM_COLOR,
    "P4" => LOW_COLOR,
    "feature" => PROPOSAL_COLOR,
    "bug" => BUG_COLOR,
    "security" => SECURITY_COLOR,
    "Community contribution" => COMMUNITY_CONTRIBUTION_COLOR,
    "backstage" => DEFAULT_COLOR,
    "Manage [DEPRECATED]" => COLOR_SCHEME[:orange],
    "Plan [DEPRECATED]" => COLOR_SCHEME[:green],
    "Create [DEPRECATED]" => COLOR_SCHEME[:yellow],
    "Package [DEPRECATED]" => COLOR_SCHEME[:purple],
    "Serverless [DEPRECATED]" => COLOR_SCHEME[:olive],
    "Fulfillment [DEPRECATED]" => COLOR_SCHEME[:teal],
    "Release [DEPRECATED]" => COLOR_SCHEME[:beige],
    "Verify [DEPRECATED]" => COLOR_SCHEME[:blue],
    "Configure [DEPRECATED]" => COLOR_SCHEME[:cyan],
    "Monitor [DEPRECATED]" => COLOR_SCHEME[:magenta],
    "Secure [DEPRECATED]" => COLOR_SCHEME[:lime],
    "Distribution [DEPRECATED]" => COLOR_SCHEME[:pink],
    "Gitaly [DEPRECATED]" => COLOR_SCHEME[:teal],
    "Geo [DEPRECATED]" => COLOR_SCHEME[:lavender],
    "Quality" => COLOR_SCHEME[:maroon],
    "Gitter [DEPRECATED]" => COLOR_SCHEME[:brown],
    "frontend" => COLOR_SCHEME[:mint],
    'avg' => AVG_COLOR,
    '85' => PERCENTILE_85_COLOR,
    '95' => PERCENTILE_95_COLOR,
    'closed' => CLOSED_ISSUES_COLOR
  }

  ACTIVE_TEAMS = [
    'Delivery',
    'Technical Writing',
    'Quality'
  ]

  DEPRECATED_TEAMS = [
    'Plan [DEPRECATED]',
    'Create [DEPRECATED]',
    'Manage [DEPRECATED]',
    'Verify [DEPRECATED]',
    'Configure [DEPRECATED]',
    'Package [DEPRECATED]',
    'Release [DEPRECATED]',
    'Monitor [DEPRECATED]',
    'Secure [DEPRECATED]',
    'Distribution [DEPRECATED]',
    'Gitaly [DEPRECATED]',
    'Geo [DEPRECATED]',
    'Serverless [DEPRECATED]',
    'Fulfillment [DEPRECATED]'
  ]

  # When adding stages or groups please consider updating app/assets/javascripts/application/query_strings.coffee
  DEV_STAGES = {
    'devops::manage': [
      "group::access",
      "group::import",
      "group::analytics",
      "group::compliance"
    ],
    'devops::plan': [
      "group::project management",
      "group::portfolio management",
      "group::certify"
    ],
    'devops::create': [
      "group::source code",
      "group::knowledge",
      "group::editor",
      "group::gitaly",
      "group::gitter"
    ]
  }

  CICD_STAGES = {
    'devops::verify': [
      "group::continuous integration",
      "group::runner",
      "group::testing"
    ],
    'devops::package': [
      "group::package"
    ],
    'devops::release': [
      "group::core release",
      "group::supporting capabilities"
    ]
  }

  OPS_STAGES = {
    'devops::configure': [
      "group::orchestration",
      "group::system"
    ],
    'devops::monitor': [
      "group::apm",
      "group::health"
    ]
  }

  SECURE_STAGES = {
    'devops::secure': [
      "group::static analysis",
      "group::dynamic analysis",
      "group::composition analysis"
    ]
  }

  DEFEND_STAGES = {
    'devops::defend': [
      "group::runtime application security",
      "group::threat management",
      "group::application infrastructure security"
    ]
  }

  GROWTH_STAGES = {
    'devops::growth': [
      "group::acquisition",
      "group::expansion",
      "group::conversion",
      "group::retention",
      "group::fulfillment",
      "group::telemetry"
    ]
  }

  ENABLEMENT_STAGES = {
    'devops::enablement': [
      "group::distribution",
      "group::geo",
      "group::memory",
      "group::ecosystem",
      "group::search",
      "group::database"
    ]
  }
end
