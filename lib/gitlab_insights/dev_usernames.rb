module GitlabInsights
  module DevUsernames
    DEV_USERNAME_MAPPING = {
      'idrozdov' => 'igor.drozdov',
      'shinya' => 'dosuken123',
      'ebajao' => 'patrickbajao',
      'Bob' => 'reprazent',
      'jedwardsjones' => 'jamedjo',
      'bwalker' => 'digitalmoksha',
      'kamil' => 'ayufan',
      'james' => 'jameslopez',
      'kushal' => 'kushalpandya',
      'nick' => 'nick.thomas',
      'dennistang' => 'dennis',
      'grzegorz' => 'grzesiek',
      'acaiazza' => 'nolith',
      'clement' => 'ClemMakesApps',
      'simon' => 'psimyn',
      'fatih' => 'fatihacet',
      'douglas' => 'dbalexandre',
      'mark' => 'markglenfletcher',
      'jacobvosmaer' => 'jacobvosmaer-gitlab',
      'douwe' => 'DouweM',
      'ruben' => 'rdavila',
      'gabriel' => 'brodock',
      'jvargas' => 'jivanvl',
      'phughes' => 'iamphill'
    }
  end
end
