module GitlabInsights
  module Regex
    MILESTONE_MATCHER = /\A\d+(\.\d+(\.\d+)?)?\z/
  end
end
