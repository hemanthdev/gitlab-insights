module GitlabInsights
  module LimitPeriod
    def limit_results(results, limit)
      if limit
        results.last(limit)
      else
        results
      end
    end
  end
end
