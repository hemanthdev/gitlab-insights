module GitlabInsights
  module Finders
    class DailyClosedIssuablesPerLabelFinder < PeriodicIssuablesPerLabelFinder
      include PerDay
      include ClosedState
    end
  end
end
