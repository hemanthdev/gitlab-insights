module GitlabInsights
  module Finders
    class AverageIssuablesPerMonthFinder < AverageIssuablesFinder
      include PerMonth
      include PeriodHelper

      def fields
        super << field
      end

      protected

      def group_results(results)
        results_for_attribute_by_period(results, field)
      end

      def format_results(results, opts)
        formatted_results = period_map(results)
        super(formatted_results, opts)
      end

      private

      def period_map(issuables_by_period)
        issuables_by_period.map do |period, issuables|
          hash_for_period_count(period, issuables) if period
        end.compact
      end

      def hash_for_period_count(period, period_issuables)
        {
          label: period.strftime(period_format),
          count: average_mr_count(period_issuables)
        }.with_indifferent_access
      end
    end
  end
end
