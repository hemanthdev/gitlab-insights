module GitlabInsights
  module Finders
    class IssuablesPerStateFinder < IssuablesPerFieldFinder
      include CreatedState

      protected

      def fields
        [:state]
      end
    end
  end
end
