module GitlabInsights
  module Finders
    class MonthlyMergedIssuablesPerStateFinderWithAverage < MonthlyMergedIssuablesPerStateFinder
      def format_results(results, opts)
        results = super(results, opts)
        previous_counts = []

        last_result = results.last
        results.map do |result, datasets|
          key = FIELD_TO_STATE_MAP[field]
          count = result[:elements][key]
          previous_counts.push(count) unless last_result == result

          {
            label: result[:label],
            elements: {
              'bar': [{ count: count, label: 'MRs Merged Per Month'}],
              'line': [{ count: rolling_average(previous_counts), label: 'Rolling Average' }]
            }
          }.with_indifferent_access
        end
      end
    end
  end
end
