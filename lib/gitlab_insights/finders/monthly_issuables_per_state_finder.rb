module GitlabInsights
  module Finders
    class MonthlyIssuablesPerStateFinder < PeriodicIssuablesPerStateFinder
      include PerMonth

      def fields
        super << :created_at << :closed_at
      end
    end
  end
end
