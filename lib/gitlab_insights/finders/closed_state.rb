module GitlabInsights
  module Finders
    module ClosedState
      def field
        :closed_at
      end
    end
  end
end
