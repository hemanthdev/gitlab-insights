module GitlabInsights
  module Finders
    module PerWeek
      def period_normalizer
        :beginning_of_week
      end

      def period_format
        "%d %b %y"
      end
    end
  end
end
