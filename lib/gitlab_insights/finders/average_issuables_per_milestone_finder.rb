module GitlabInsights
  module Finders
    class AverageIssuablesPerMilestoneFinder < AverageIssuablesFinder
      include IssuablesPerMilestone

      def fields
        super << :milestone
      end

      def bar_label
        'MRs Merged Per Milestone'
      end

      def line_label
        'Rolling Average'
      end

      protected

      def format_results(results, opts)
        formatted_results = results.map do |milestone, issuables|
          hash_for_milestone_count(milestone, average_mr_count(issuables)) if milestone
        end.compact

        formatted_results = pick_milestones(formatted_results)

        super(formatted_results, opts)
      end
    end
  end
end
