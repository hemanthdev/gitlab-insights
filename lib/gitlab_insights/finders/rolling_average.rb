module GitlabInsights
  module Finders
    module RollingAverage
      PERIODS_FOR_AVG = 6

      def rolling_average(previous_counts)
        if previous_counts.length == PERIODS_FOR_AVG
          avg = previous_counts.reduce(:+) / previous_counts.size.to_f
          previous_counts.shift
          avg.round(2)
        end
      end
    end
  end
end
