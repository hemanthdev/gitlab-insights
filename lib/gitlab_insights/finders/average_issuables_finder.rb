module GitlabInsights
  module Finders
    class AverageIssuablesFinder < IssuablesPerFieldFinder
      def field
        :iid
      end

      def fields
        [:author]
      end

      protected

      def format_results(results, opts)
        previous_counts = []

        last_result = results.last
        results.map do |result, datasets|
          previous_counts.push(result[:count]) unless last_result == result

          {
            label: result[:label],
            elements: {
              'bar': [ { label: bar_label, count: result[:count] } ],
              'line': [ { label: line_label, count: rolling_average(previous_counts) } ]
            }
          }.with_indifferent_access
        end
      end

      private

      def average_mr_count(issuables)
        uniq_authors = issuables.map do |issuable|
          issuable[:author].with_indifferent_access[:username]
        end.uniq

        (issuables.length.to_f / uniq_authors.length.to_f).round(2)
      end
    end
  end
end
