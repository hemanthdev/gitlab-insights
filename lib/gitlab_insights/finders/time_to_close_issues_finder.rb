module GitlabInsights
  module Finders
    class TimeToCloseIssuesFinder < MonthlyClosedIssuablesPerLabelFinder
      NUM_MONTHS = 24

      include PeriodHelper

      def fields
        super << :created_at
      end

      def group_results(results)
        results = results_for_attribute_by_period(results, field)
      end

      def format_results(results, opts)
        time_to_close_results = calculate_time_to_close_all_periods(results)

        time_to_close_results.map do |period, data|
          dataset_for_period(period, data)
        end
      end

      private

      def dataset_for_period(period, data)
        {
          label: period,
          elements: {
            'bar': [
              { label: 'avg', count: data[:avg] },
              { label: '85', count: data[:eight_five] },
              { label: '95', count: data[:nine_five] }
            ],
            'line':
            [
              { label: 'closed', count: data[:closed]}
            ]
          }
        }.with_indifferent_access
      end

      def calculate_time_to_close_all_periods(results)
        #the `closed_at` field was introduced around April 2017
        #it is possible for closed issues to have no `closed_at`, remove these
        results.delete(nil)
        results = backfill_empty_periods(results)

        period_calculation_hash = results.each_with_object({}) do |(label, issuables), hash|
          formatted_period = label.strftime(period_format)
          hash[formatted_period] = calculated_data_for_period(issuables)
        end

        #only take last 2 years worth of data, like the gsheet
        reduce_periods(period_calculation_hash)
      end

      def calculated_data_for_period(issuables)
        {
          avg: average_time_to_close_in_days(issuables).round(1),
          eight_five: percentile_time_to_close_in_days(85, issuables).round(1),
          nine_five: percentile_time_to_close_in_days(95, issuables).round(1),
          closed: issuables.count
        }
      end

      def time_to_close_in_days_collection(issuables)
        issuables.map do |issuable|
          ((issuable.closed_at - issuable.created_at) / 1.day)
        end.sort
      end

      def average_time_to_close_in_days(issuables)
        time_to_close_numbers = time_to_close_in_days_collection(issuables)
        return 0 if time_to_close_numbers.empty?
        time_to_close_numbers.sum / time_to_close_numbers.length
      end

      def percentile_time_to_close_in_days(percentile, issuables)
        values = time_to_close_in_days_collection(issuables).sort
        values.percentile(percentile) || 0
      end

      def backfill_empty_periods(periods)
        (0...NUM_MONTHS).reverse_each.each_with_object(periods) do |period_ago, hash|
          month = period_ago.public_send('month').ago.send(period_normalizer)
          hash[month] ||= []
        end
      end

      def reduce_periods(periods)
        periods.sort do |(a,v1),(b,v2)|
          DateTime.parse(a) <=> DateTime.parse(b)
        end.last(NUM_MONTHS).to_h
      end
    end
  end
end
