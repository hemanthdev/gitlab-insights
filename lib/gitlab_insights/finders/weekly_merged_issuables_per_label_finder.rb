module GitlabInsights
  module Finders
    class WeeklyMergedIssuablesPerLabelFinder < PeriodicIssuablesPerLabelFinder
      include PerWeek
      include MergedState
    end
  end
end
