module GitlabInsights
  module Finders
    module IssuablesPerMilestone
      MIN_MILESTONE = "10.0"
      MILESTONE_PATCH_SUFFIX = '.0'

      protected

      def group_results(results)
        issuables_by_milestone = results.group_by { |result| result.dig(:milestone, "title") }
      end

      private

      def custom_filter_results(results, opts)
        super.where.not(milestone: nil)
      end

      def hash_for_milestone_count(milestone, count)
        {
          label: milestone,
          count: count
        }.with_indifferent_access
      end

      def pick_milestones(results)
        matching_milestones = results.select do |result|
          result[:label] =~ GitlabInsights::Regex::MILESTONE_MATCHER
        end

        sort_milestones(matching_milestones)
      end

      def sort_milestones(milestones)
        milestones.sort! do |a, b|
          semantic_milestone(a) <=> semantic_milestone(b)
        end

        milestones.select do |milestone|
          sem_ver = semantic_milestone(milestone)
          sem_ver && sem_ver >= Semantic::Version.new(min_milestone)
        end
      end

      def min_milestone
        MIN_MILESTONE + MILESTONE_PATCH_SUFFIX
      end

      def semantic_milestone(milestone)
        Semantic::Version.new(milestone[:label])
      rescue ArgumentError
        begin
          Semantic::Version.new(milestone[:label] + MILESTONE_PATCH_SUFFIX)
        rescue
          nil
        end
      end
    end
  end
end
