module GitlabInsights
  module Finders
    module MergedState
      def field
        :merged_at
      end
    end
  end
end
