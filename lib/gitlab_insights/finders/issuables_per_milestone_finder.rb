module GitlabInsights
  module Finders
    class IssuablesPerMilestoneFinder < IssuablesPerLabelFinder
      include IssuablesPerMilestone

      def fields
        super << :milestone
      end

      protected

      def format_results(results, opts)
        formatted_results = milestone_map(results, opts[:collection_labels])

        pick_milestones(formatted_results)
      end

      def milestone_map(issuables_by_milestone, labels)
        issuables_by_milestone.map do |milestone, issuables|
          hash_for_milestone_count(milestone, issuables, labels) if milestone
        end.compact
      end

      def hash_for_milestone_count(milestone, milestone_issuables, labels)
        {
          label: milestone,
          elements: issuables_with_label_count(milestone_issuables, labels)
        }.with_indifferent_access
      end
    end
  end
end
