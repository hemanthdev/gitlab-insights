require 'digest/md5'

module GitlabInsights
  module Presenters
    module Chartjs
      class Presenter

        # To be overridden in specific classes
        # Results are expected to be in the following format:
        # [
        #   {
        #     label: 'string',
        #     elements:
        #       {
        #         'name': count
        #         ... other presenter specific fields
        #       }
        #   }
        # ]
        def present(results)
          raise NotImplementedError
        end

        def generate_color_code(label)
          GitlabInsights::STATIC_COLOR_MAP[label] || "##{Digest::MD5.hexdigest(label.to_s)[0..5]}"
        end

        def default_color
          GitlabInsights::STATIC_COLOR_MAP[GitlabInsights::UNCATEGORIZED]
        end

        def chart_data_format(labels, raw_datasets)
          {}.tap do |chart|
            chart[:labels] = labels
            chart[:datasets] = raw_datasets.map do |label, data|
              dataset_format(label, data, generate_color_code(label))
            end
          end.with_indifferent_access
        end
      end
    end
  end
end
