module GitlabInsights
  module Presenters
    module Chartjs
      class MrsPerMonthPresenter < IssuablesPerMilestonePresenter
        def bar_label
          "MRs Merged Per Month"
        end
      end
    end
  end
end
