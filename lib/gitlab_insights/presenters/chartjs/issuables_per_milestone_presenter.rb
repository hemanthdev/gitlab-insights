module GitlabInsights
  module Presenters
    module Chartjs
      class IssuablesPerMilestonePresenter < ComboBarLinePresenter
        def default_colors
          true
        end

        def line_color
          GitlabInsights::LINE_COLOR
        end

        def line_width
          2
        end

        def bar_color(data, datum)
          datum == data.last ? GitlabInsights::COLOR_SCHEME[:apricot] : GitlabInsights::DEFAULT_COLOR
        end
      end
    end
  end
end
